﻿// See https://aka.ms/new-console-template for more information
using System.Diagnostics;
using System.Reflection.Metadata.Ecma335;

Console.WriteLine("Hello, World!");



long CalcRegSum(long[] source)
{
    long sum = 0;
    Stopwatch sw = Stopwatch.StartNew();
    sw.Start();
    for (int i = 0; i < source.Length; i++) sum+= source[i];
    sw.Stop();
    Console.WriteLine($"Regular sum calculation for {source.Length} items took {sw.Elapsed.TotalMilliseconds} ms, sum is {sum}");
    return sum;
}
long CalcThreadSum(long[] source, int thrCnt)
{
    
    long sum = 0;
    int range = source.Length / thrCnt;
    int rem = source.Length - thrCnt * range;
    Thread[] threads = new Thread[thrCnt + (rem > 0 ? 1 : 0)];
    long[] results = new long[thrCnt + (rem > 0 ? 1 : 0)];
    void calcSum(object so)
    {
        int sp = (so as SO).startPos;
        int ep = (so as SO).endPos;
        int resIdx = (so as SO).idx;
        for (int i =sp; i <ep ; i++ ) results[resIdx] += source[i];
    }
    Stopwatch sw = Stopwatch.StartNew();
    sw.Start();
    for (int i = 0; i < thrCnt; i++)
    {
        threads[i] = new Thread(calcSum);
        var so = new SO() { idx = i,  startPos = i*range, endPos = i*range + range };
        threads[i].Start(so);
    }
    if (rem > 0) {
        threads[thrCnt] = new Thread(calcSum);
        var so = new SO() { idx = thrCnt, startPos = thrCnt*range, endPos = thrCnt*range + rem };
        threads[thrCnt].Start(so);
    }
    for(int i = 0;i < threads.Length; i++) threads[i].Join();
    for (int i = 0; i < threads.Length; i++) sum += results[i];
    sw.Stop();
    Console.WriteLine($"Thread calculation for {source.Length} itemx took {sw.Elapsed.TotalMilliseconds} ms, sum is {sum}");
    return sum;
}

long CalcParallelSum(long[] source, int parallelCnt)
{
    long sum = 0;
    Stopwatch sw = Stopwatch.StartNew();
    sw.Start();
    sum = source.AsParallel().Sum(x=> { return x; });
    sw.Stop();
    Console.WriteLine($"Parallel calculation for {source.Length} itemx took {sw.Elapsed.TotalMilliseconds} ms, sum is {sum}");
    return sum;
}

long[] GenerateIntArray(int length)
{
    long[] array = new long[length];
    var rnd = new Random();
    for(int i = 0; i < length;i++)
        array[i] = (int)rnd.NextInt64(1000);
    return array;
}

Console.WriteLine("Generate 100003 items array");
var source = GenerateIntArray(100003);
CalcRegSum(source);
CalcThreadSum(source, 8);
CalcParallelSum(source, 8);

Console.WriteLine("Generate 1000003 items array");
source = GenerateIntArray(1000003);
CalcRegSum(source);
CalcThreadSum(source, 8);
CalcParallelSum(source, 8);

Console.WriteLine("Generate 10000003 items array");
source = GenerateIntArray(10000003);
CalcRegSum(source);
CalcThreadSum(source, 8);
CalcParallelSum(source, 8);


class SO
{
    public int idx;
    public int startPos;
    public int endPos;
}
